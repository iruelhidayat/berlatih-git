<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    // echo "<h1>Release 0</h1>";
    $sheep = new Animal("Shaun");
    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    
    // echo "<h1>Release 0</h1>";
    $kodok = new Frog("Buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
    $kodok->jump();// "hop hop" 
    echo"<br><br>";
    
    $sungokong = new Ape("Kera Sakti");
    echo "Name : " . $sungokong->name . "<br>";
    echo "Legs : " . $sungokong->legs . "<br>";
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
    $sungokong->yell(); // "Auooo"
    echo"<br><br>";
    
?>