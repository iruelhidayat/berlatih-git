@extends('layout.master')

@section('judul')
    Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label> <br>
        <input type="text" name="firstname"> <br><br>

        <label>Last Name:</label> <br>
        <input type="text" name="lastname"> <br><br>

        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="male" >Male <br>
        <input type="radio" name="gender" value="female" >Female <br>
        <input type="radio" name="gender" value="other" >Other <br><br>

        <label>Nationality:</label> <br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singapore</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Australia</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="language" value="indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="english">English <br>
        <input type="checkbox" name="language" value="other">Other <br><br>

        <label>Bio :</label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="SignUp">
    </form>
@endsection
