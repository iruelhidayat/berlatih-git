<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function() {
    echo "Halooooo apa kabar <br>";
    return "Bismillah Belajar Go Laravel";
});

Route::get('/nama/{name}', function($name){
    return "Helooo $name";
});

Route::get('/pegawai', 'PegawaiController@index');
// Route::get('/submit', 'PegawaiController@submit');
Route::post('/submit', 'PegawaiController@submit');
*/

/* Intro Laravel*/
Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@send');

/* Templating Laravel*/
Route::get('/master', function() {
    return view('layout.master');
});

Route::get('/table', function() {
    return view('table');
});

Route::get('/data-table', function() {
    return view('datatable');
});

// Form Cast
Route::get('/cast/create', 'CastController@create');

// Send data ke table cast
Route::post('/cast', 'CastController@store');

// Tampil Data Cast
Route::get('/cast', 'CastController@index');

// Detail Cast
Route::get('/cast/{id}', 'CastController@show');

// Form Edit Data Cast
Route::get('/cast/{id}/edit', 'CastController@edit');
// Edit Data Cast
Route::put('/cast/{id}', 'CastController@update');

// Delete Cast

// Delete ID Cast
Route::delete('/cast/{id}', 'CastController@destroy');