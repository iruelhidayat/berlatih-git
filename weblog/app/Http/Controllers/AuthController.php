<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {
    public function register(){
        return view('register');
    }

    public function send(Request $req){
        // dd($req->all());
        $namadepan = $req["firstname"];
        $namabelakang = $req["lastname"];
        return view('congrats', compact('namadepan','namabelakang'));
    }
}
